<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function index()
    {
        $users = User::whereNull('approved_at')->get();

        return view('auth.users', compact('users'));
    }

    public function showList()
    {
        $users = User::all();

        return view('users.list', compact('users'));
    }

    public function showForm()
    {
        //$users = User::all();

        return view('users.form');
    }

    public function showDetail($user_id)
    {
        $user = User::findOrFail($user_id);
        //$user->update(['approved_at' => now()]);

        return view('users.details', compact('users'));
    }

    

    public function approve($user_id)
    {
        $user = User::findOrFail($user_id);
        $user->update(['approved_at' => now()]);

        return redirect()->route('admin.auth.users.index')->withMessage('User approved successfully');
    }
}
