<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = Auth::id();
        return view('home',compact('users'));
    }

    public function profile()
    {
        return view('profile');
    }

    /**
     * Show the approval dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */    
    public function approval()
    {
        return view('auth.approval');
    }
}
