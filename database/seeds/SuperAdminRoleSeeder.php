<?php

use Illuminate\Database\Seeder;

class SuperAdminRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = App\User::findOrFail('1');
        $user->assignRole('super-admin');
        $user = App\User::findOrFail('2');
        $user->assignRole('writer');
    }
}
