<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::middleware(['auth','verified'])->group(function () {
    Route::get('/approval', 'HomeController@approval')->name('approval');

    Route::middleware(['role:super-admin'])->group(function () {
        Route::get('/users/approval', 'UserController@index')->name('admin.auth.users.approval');
        Route::get('/users/{user_id}/approve', 'UserController@approve')->name('admin.auth.users.approve');
        Route::get('/users/list', 'UserController@showList')->name('admin.auth.users.list');
        Route::get('/users/create', 'UserController@showForm')->name('admin.auth.users.create');
        Route::get('/users/{user_id}/view', 'UserController@showDetail')->name('admin.auth.users.view');
    });

    Route::middleware(['approved'])->group(function () {
        Route::get('/home', 'HomeController@index')->name('home');

        Route::middleware(['role:writer'])->group(function () {
            Route::get('/home/profile', 'HomeController@profile')->name('profile');
        


        });
    });
});



