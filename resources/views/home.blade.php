@extends('layouts.app')

@section('content')
<div class="row purchace-popup">
        <div class="col-12">
          <span class="d-block d-md-flex align-items-center">

            @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
            @endif
            @if (session('message'))
                    <div class="alert alert-danger" role="alert">
                        {{ session('message') }}
                    </div>
            @endif    
            <p>You are logged in!</p>
            <a class="btn ml-auto download-button d-none d-md-block" href="#" target="_blank">Test</a>
            <a class="btn purchase-button mt-4 mt-md-0" href="#" target="_blank">
                @role('super-admin', 'web')
                    I am a super-admin!
                    @else
                    I am not a super-admin...
                    @endrole</a>
            <i class="mdi mdi-close popup-dismiss d-none d-md-block"></i>
          </span>
        </div>
      </div>
@endsection
