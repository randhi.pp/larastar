@extends('layouts.app') 
@section('content')
<div class="row">
  <div class="col-md-6 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Blockquotes</h4>
        <p class="card-description">
          Wrap content inside
          <code>&lt;blockquote class="blockquote"&gt;</code>
        </p>
        <blockquote class="blockquote">
          <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
        </blockquote>
      </div>
      <div class="card-body">
        <blockquote class="blockquote blockquote-primary">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
          <footer class="blockquote-footer">Someone famous in
            <cite title="Source Title">Source Title</cite>
          </footer>
        </blockquote>
      </div>
    </div>
  </div>
  <div class="col-md-6 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Address</h4>
        <p class="card-description">
          Use
          <code>&lt;address&gt;</code> tag
        </p>
        <div class="row">
          <div class="col-md-6">
            <address>
                    <p class="font-weight-bold">Star Admin Inc</p>
                    <p>
                      695 lsom Ave,
                    </p>
                    <p>
                      Suite 00
                    </p>
                    <p>
                      San Francisco, CA 94107
                    </p>
                  </address>
          </div>
          <div class="col-md-6">
            <address class="text-primary">
                    <p class="font-weight-bold">
                      E-mail
                    </p>
                    <p class="mb-2">
                      johndoe@examplemeail.com
                    </p>
                    <p class="font-weight-bold">
                      Web Address
                    </p>
                    <p>
                      www.staradmin.com
                    </p>
                  </address>
          </div>
        </div>
      </div>
      <div class="card-body">
        <h4 class="card-title">Lead</h4>
        <p class="card-description">
          Use class
          <code>.lead</code>
        </p>
        <p class="lead">
          Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.
        </p>
      </div>
    </div>
  </div>
</div>
@endsection