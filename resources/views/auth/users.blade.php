@extends('layouts.app') 
@section('content')
<div class="row">
  <div class="col-12 grid-margin">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title mb-4">Users List to Approve</h5>
        <div class="fluid-container">
          @if (session('message'))
          <div class="alert alert-success" role="alert">
            {{ session('message') }}
          </div>
          @endif @forelse ($users as $user)
          <div class="row ticket-card mt-3 pb-2 border-bottom pb-3 mb-3">
            <div class="col-md-1">
              <img class="img-sm rounded-circle mb-4 mb-md-0" src="{{ url($user->photo_url ? $user->photo_url : $user->gravatar ) }}" alt="profile image">
            </div>
            <div class="ticket-details col-md-9">
              <div class="d-flex">
                <p class="text-dark font-weight-semibold mr-2 mb-0 no-wrap">{{ $user->name }}</p>
                <p class="text-primary mr-1 mb-0">[{{ $user->id }}]</p>
                <p class="mb-0 ellipsis">{{ $user->email }}</p>
              </div>
              <!-- <p class="text-gray ellipsis mb-2">Donec rutrum congue leo eget malesuada. Quisque velit nisi, pretium ut lacinia in, elementum id enim
                          vivamus.
                        </p> -->
              <div class="row text-gray d-md-flex d-none">
                <div class="col-4 d-flex">
                  <small class="mb-0 mr-2 text-muted text-muted">Verified :</small>
                  <small class="Last-responded mr-2 mb-0 text-muted text-muted">{{ $user->email_verified_at }}</small>
                </div>
                <!-- <div class="col-4 d-flex">
                            <small class="mb-0 mr-2 text-muted text-muted">Due in :</small>
                            <small class="Last-responded mr-2 mb-0 text-muted text-muted">2 Days</small>
                          </div> -->
              </div>
            </div>
            <div class="ticket-actions col-md-2">
              <div class="btn-group dropdown">
                <button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Manage
                          </button>
                <div class="dropdown-menu">
                  <a class="dropdown-item" href="{{ route('admin.auth.users.approve', $user->id) }}">
                              <i class="fa fa-reply fa-fw"></i>Approve User</a>
                  <a class="dropdown-item" href="#">
                              <i class="fa fa-history fa-fw"></i>Delete User</a>
                  <div class="dropdown-divider"></div>
                  <!-- <a class="dropdown-item" href="#">
                              <i class="fa fa-check text-success fa-fw"></i>Resolve Issue</a>
                            <a class="dropdown-item" href="#">
                              <i class="fa fa-times text-danger fa-fw"></i>Close Issue</a> -->
                </div>
              </div>
            </div>
          </div>
          @empty
          <div class="row ticket-card mt-3 pb-2 border-bottom pb-3 mb-3">
            <div class="ticket-details col-md-12">
              <p>No users found.</p>
            </div>
          </div>
          @endforelse
        </div>
      </div>
    </div>
  </div>
</div>
@endsection