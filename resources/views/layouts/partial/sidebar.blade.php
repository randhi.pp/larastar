<nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <div class="nav-link">
              <div class="user-wrapper">
                <div class="profile-image">
                  <img src="{{ url(Auth::user()->photo_url ? Auth::user()->photo_url : Auth::user()->gravatar )}}" alt="profile image">
                </div>
                <div class="text-wrapper">
                  <p class="profile-name">{{ ucwords(Auth::user()->name) }}</p>
                  <div>
                    <small class="designation text-muted">{{ ucwords(Auth::user()->getRoleNames()) }}</small>
                    <span class="status-indicator online"></span>
                  </div>
                </div>
              </div>
              <button class="btn btn-success btn-block">New Project
                <i class="mdi mdi-plus"></i>
              </button>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('home') }}">
              <i class="menu-icon mdi mdi-television"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li>
          {{-- <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-content-copy"></i>
              <span class="menu-title">Basic UI Elements</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="pages/ui-features/buttons.html">Buttons</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="pages/ui-features/typography.html">Typography</a>
                </li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="pages/forms/basic_elements.html">
              <i class="menu-icon mdi mdi-backup-restore"></i>
              <span class="menu-title">Form elements</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="pages/charts/chartjs.html">
              <i class="menu-icon mdi mdi-chart-line"></i>
              <span class="menu-title">Charts</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="pages/tables/basic-table.html">
              <i class="menu-icon mdi mdi-table"></i>
              <span class="menu-title">Tables</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="pages/icons/font-awesome.html">
              <i class="menu-icon mdi mdi-sticker"></i>
              <span class="menu-title">Icons</span>
            </a>
          </li> --}}
        {{-- {{ preg_match("~users/(\d+)/view~", Request::path()) }} --}}
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#users" aria-expanded="false" aria-controls="users">
              <i class="menu-icon mdi mdi-restart"></i>
              <span class="menu-title">Manage Users</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="{{ Request::path() == 'users/approval' ||
                           Request::path() == 'users/list' ||
                           preg_match("~users/(\d+)/view~", Request::path()) ||
                           Request::path() == 'users/create' ? '' : 'collapse' }}" id="users">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link {{ $routeName == 'admin.auth.users.list' ||
                  preg_match("~users/(\d+)/view~", Request::path()) ? 'active' : '' }}" href="{{ route('admin.auth.users.list') }}"> User List </a>
                </li>
                @role('super-admin', 'web')
                <li class="nav-item">
                <a class="nav-link {{ $routeName == 'admin.auth.users.approval' ? 'active' : '' }}" href="{{ route('admin.auth.users.approval') }}"> Approval </a>
                </li>
                @endrole
                <li class="nav-item">
                  <a class="nav-link {{ $routeName == 'admin.auth.users.create' ? 'active' : '' }}" href="{{ route('admin.auth.users.create') }}"> Create </a>
                </li>
              </ul>
            </div>
          </li>
        </ul>
      </nav>
      