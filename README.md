**LARASTAR**

Project Boilerplate / Template untuk Laravel 5.8 dengan menggunakan Star Admin Panel

Version : `initial / beta`

![](screenshot/screenshoot_(1).png)
![](screenshot/screenshoot_(2).png)
![](screenshot/screenshoot_(3).png)
![](screenshot/screenshoot_(4).png)
![](screenshot/screenshoot_(5).png)
![](screenshot/screenshoot_(6).png)
![](screenshot/screenshoot_(7).png)


Include & Working :

* Register User baru => email harus verified & manual approval dari admin
* Spatie/permission => role based

How to:
`Pastikan pc anda sudah memiliki dev tool untuk laravel`



1. Clone git ini.

* `git clone https://gitlab.com/randhi.pp/larastar `
* `composer install`
* `npm install`

2. set .env 
* Set DB dengan nama database, user dan password anda.
* set SMTP setting, bisa pakai gmail / mailtrap ( bikin gratis )
* buka terminal dan jalankan `php artisan migrate --seed`

Super Admin

* user: admin@admin.com
* pasw: admin

normal user

* user: guest@guest.com
* pasw: Guest1234

